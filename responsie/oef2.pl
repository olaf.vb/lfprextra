

priem(N) :- N > 1, N1 is N - 1, priem_h(N, N1).
priem_h(N, 1) :- !, N > 1.
priem_h(N, N1) :- Y is N mod N1, Y \= 0, X is N1 - 1, priem_h(N, X).


g_helper(G, G, _) :- !, fail.
g_helper(G, N, [N, M]) :- M is G - N, priem(N), priem(M).
g_helper(G, N, R) :- N1 is N + 1, g_helper(G, N1, R).   

goldbach(G, L) :- g_helper(G, 2, L).