split(L, L1, L2) :- append(L1, L2, L), L1 \= [], L2 \= [].


make_t([H], H).
make_t([H1, H2|T], R) :- make_t([H1 + H2|T], R).
make_t([H1, H2|T], R) :- make_t([H1 - H2|T], R).
make_t([H1, H2|T], R) :- make_t([H1 / H2|T], R).
make_t([H1, H2|T], R) :- make_t([H1 * H2|T], R).

make_eq(L, LT, RT) :- split(L, L1, L2), make_t(L1, LT), make_t(L2, RT), LT =:= RT.