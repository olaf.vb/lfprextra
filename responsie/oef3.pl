conc([],L,L).
conc([X|L1], L2, [X|L3]) :- conc(L1, L2, L3).


boom(Trace, Tree) :- b_helper(Trace, Tree, []).


b_helper([H, *|T], t(H, []), T).
b_helper([H1, H2|T], Tree ,R) :- 
                            H2 \= '*',
                            b_helper([H2|T], C, Rest),
                            b_helper([H1|Rest], t(H1, Children), R),
                            Tree = t(H1, [C|Children]).


