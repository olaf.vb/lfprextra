maak_vgl(G, A, B) :- maak_vgl_h(G, LT, RT), symbool(LT, A), symbool(RT, B), C is A, D is B, C = D. 

% verdeel in alle mogelijke combinaties van linker- en rechterlid 
maak_vgl_h([IH|IT], [IH], IT) :- IT \= [].
maak_vgl_h([IH|IT], [IH|R], N) :- maak_vgl_h(IT, R, N).

% maak alle mogelijke combinaties van haakjes en symbolen
symbool([H|T], H) :- T = [].
symbool(H, Re) :- maak_vgl_h(H, L, R), symbool(L, LR), symbool(R, RR), Re = LR * RR.
symbool(H, Re) :- maak_vgl_h(H, L, R), symbool(L, LR), symbool(R, RR), Re = LR + RR.
symbool(H, Re) :- maak_vgl_h(H, L, R), symbool(L, LR), symbool(R, RR), Re = LR - RR.
symbool(H, Re) :- maak_vgl_h(H, L, R), symbool(L, LR), symbool(R, RR), Re = LR / RR.



