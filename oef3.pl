conc([],L,L).
conc([X|L1], L2, [X|L3]) :- conc(L1, L2, L3).


subBoom(Trace, Sub, C) :- conc([X], S, Trace), X \= '*', C1 is C + 1, subBoom(S, OSub, C1), Sub = [X|OSub]. 
subBoom(Trace, Sub, C) :- conc(['*'], S, Trace), C > 1, C1 is C - 1, subBoom(S, OSub, C1), Sub = ['*'|OSub]. 
subBoom(Trace, ['*'], C) :- conc(['*'], S, Trace), C = 1.


subBoomen([], []).
subBoomen(Trace, [L|NL]) :- subBoom(Trace, SubTrace, 0), boom(SubTrace, L), conc(SubTrace, Rest, Trace), subBoomen(Rest, NL), !.


boom(Trace, t(X,[])) :- conc([X],S,Trace), conc(Tsub, ['*'], S), Tsub = []. 
boom(Trace, t(X,N)) :- conc([X],S,Trace), conc(Tsub, ['*'], S), subBoomen(Tsub, N).