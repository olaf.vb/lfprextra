

priem(X) :- X > 1, N is X - 1, priem(X, N).
priem(X, 1).
priem(X, N) :- A is X mod N, A \= 0, N1 is N - 1, priem(X, N1).    

smaller([H|T], G) :- G > 2, H is G - 1, smaller(T, H).
smaller([1], G) :- G = 2.


goldbach(G, [A|[B]]) :- smaller(S, G), member(A, S), member(B, S), priem(A), priem(B), G is A + B.



