conc([],L,L).
conc([X|L1], L2, [X|L3]) :- conc(L1, L2, L3).


boom(Trace, t(A, L)) :- conc(T, ['*'], Trace), conc([A], Remainder, T), subtrees(Remainder, L).

subtrees([], []).
subtrees(Trace, [Tree]) :- subTrace(Trace, Result, 0), conc(Result, New, Trace), New = [], boom(Result, Tree).
subtrees(Trace, [Tree|TR]) :- subTrace(Trace, Result, 0), conc(Result, New, Trace), New \= [], subtrees(New, TR), boom(Result, Tree).


subTrace(Trace, ['*'], C) :- conc(['*'], Remainder, Trace), C = 1.
subTrace(Trace, ['*'|NSub], C) :- conc(['*'], Remainder, Trace), C1 is C - 1, C1 > 0, subTrace(Remainder, NSub, C1).
subTrace(Trace, [A|NSub], C) :- conc([A], Remainder, Trace), A \= '*', C1 is C + 1, subTrace(Remainder, NSub, C1).