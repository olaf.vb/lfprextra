maak_vgl(I, LL, RL) :- splits(I, L, R), symbool(L, LL), symbool(R, RL), A is LL, B is RL, A = B.


splits([H|T], [H], T) :- T \= [].
splits([H|T], [H|TL], TR) :- splits(T, TL, TR).  


symbool([X], X).
symbool(I, Out) :- splits(I, L, R), symbool(L, NL), symbool(R, NR), Out = NL / NR.
symbool(I, Out) :- splits(I, L, R), symbool(L, NL), symbool(R, NR), Out = NL * NR.
symbool(I, Out) :- splits(I, L, R), symbool(L, NL), symbool(R, NR), Out = NL + NR.
symbool(I, Out) :- splits(I, L, R), symbool(L, NL), symbool(R, NR), Out = NL - NR.