

priem(N) :- N > 1, N1 is N - 1, priem_h(N, N1).
priem_h(N, 1) :- !, N > 1.
priem_h(N, N1) :- Y is N mod N1, Y \= 0, X is N1 - 1, priem_h(N, X).

smaller(N, R) :- N > 1, R is N - 1.
smaller(N, Out) :- R is N - 1, R > 1, smaller(R, Out).

goldbach(N, [A|[B]]) :- smaller(N, A), smaller(N, B), priem(A), priem(B), N is A + B.